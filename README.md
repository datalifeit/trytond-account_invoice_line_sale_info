datalife_account_invoice_line_sale_info
=======================================

The account_invoice_line_sale_info module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-account_invoice_line_sale_info/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-account_invoice_line_sale_info)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
