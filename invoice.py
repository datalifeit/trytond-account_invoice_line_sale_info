# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Equal, Eval, Not, And
from trytond.transaction import Transaction
from sql import Null, Literal
from sql.operators import Concat
from sql.conditionals import Case


class InvoiceLine(metaclass=PoolMeta):
    __name__ = 'account.invoice.line'

    shipment_out_numbers = fields.Function(
        fields.Char('Shipment Out Numbers'),
        'get_shipment_out_numbers', searcher='search_shipment_out_numbers')
    sale_date = fields.Function(
        fields.Date('Sale date'), 'get_sale_data', searcher='search_sale_data')

    @classmethod
    def order_sale_date(cls, tables):
        line, _ = tables[None]

        # it allows to get different origins which are related in some way
        # with a sale
        fromitem, conditions = cls._get_order_sale_date_query_items(line)
        query = fromitem.select(
            line.id,
            Case(*conditions, _else=Null),
            order_by=Case(*conditions, _else=Null)
        )

        cursor = Transaction().connection.cursor()
        cursor.execute(*query)
        dates = cursor.fetchall()

        conditions = []
        for x, values in enumerate(dates):
            line_id, date = values
            conditions.append((line.id == line_id, date))

        if not conditions:
            conditions.append((Literal(True), Null))

        return [Case(*conditions, _else=Null)]

    @classmethod
    def _get_order_sale_date_query_items(cls, table):
        pool = Pool()
        Sale = pool.get('sale.sale')
        SaleLine = pool.get('sale.line')
        sale = Sale.__table__()
        sale_line = SaleLine.__table__()

        fromitem = table.join(sale_line, 'LEFT', condition=(
                Concat('sale.line,', sale_line.id) == table.origin)
            ).join(sale, 'LEFT', condition=(sale_line.sale == sale.id))
        conditionals = [(sale.sale_date != Null, sale.sale_date)]
        return fromitem, conditionals

    @classmethod
    def order_shipment_out_numbers(cls, tables):
        line, _ = tables[None]
        fromitem, conditions = cls._get_order_shipment_out_numbers_query_items(
            line)
        query = fromitem.select(
                line.id,
                Case(*conditions, _else=Literal('')),
                order_by=Case(*conditions, _else=Literal('')),
                group_by=(line.id, Case(*conditions, _else=Literal('')))
        )

        cursor = Transaction().connection.cursor()
        cursor.execute(*query)
        numbers = cursor.fetchall()

        conditions = []
        lines = {}
        for x, values in enumerate(numbers):
            # group by line
            line_id, number = values
            if number:
                lines.setdefault(line_id, []).append(number)

        for line_id, numbers in lines.items():
            conditions.append((line.id == line_id, ','.join(sorted(numbers))))

        if not conditions:
            conditions.append((Literal(True), Null))

        return [Case(*conditions, _else=Null)]

    @classmethod
    def _get_order_shipment_out_numbers_query_items(cls, table):
        pool = Pool()
        StockMove = pool.get('stock.move')
        InvoiceMove = pool.get('account.invoice.line-stock.move')
        Shipmentout = pool.get('stock.shipment.out')
        invoice_move = InvoiceMove.__table__()
        stock_move = StockMove.__table__()
        shipment_out = Shipmentout.__table__()

        fromitem = table.join(invoice_move, 'LEFT', condition=(
                invoice_move.invoice_line == table.id)
            ).join(stock_move, 'LEFT', condition=(
                invoice_move.stock_move == stock_move.id)
            ).join(shipment_out, 'LEFT', condition=(
                Concat('stock.shipment.out,', shipment_out.id
                    ) == stock_move.shipment)
        )
        conditionals = [(shipment_out.number != Null, shipment_out.number)]
        return fromitem, conditionals

    def get_shipment_out_numbers(self, name=None):
        if self.stock_moves:
            shipments = set([move.shipment.rec_name
                for move in self.stock_moves if move.shipment
                    and move.shipment.__name__ == 'stock.shipment.out'])
            return ','.join(sorted(shipments))

    @classmethod
    def search_shipment_out_numbers(cls, name, clause):
        return [('stock_moves.shipment.number',)
            + tuple(clause[1:3]) + ('stock.shipment.out',) + tuple(clause[3:])]

    def get_sale_data(self, name=None):
        pool = Pool()
        SaleLine = pool.get('sale.line')

        if isinstance(self.origin, SaleLine):
            return getattr(self.origin.sale, name)

    @classmethod
    def search_sale_data(cls, name, clause):
        return [('origin.sale.%s' % clause[0],)
            + tuple(clause[1:3]) + ('sale.line',) + tuple(clause[3:])]

    @classmethod
    def view_attributes(cls):
        return super(InvoiceLine, cls).view_attributes() + [
            ('/tree/field[@name="shipment_out_numbers"]', 'tree_invisible',
                And(Not(Equal(Eval('invoice_type', ''), 'out')),
                    Not(Equal(Eval('type', ''), 'out')))),
            ('/tree/field[@name="sale_date"]', 'tree_invisible',
                And(Not(Equal(Eval('invoice_type', ''), 'out')),
                    Not(Equal(Eval('type', ''), 'out')))),
            ]
